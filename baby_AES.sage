# von Ioannis Charitos
# um baby aes auszuprobieren anhand von dem Beispiel vom Buch:
#
# sage: load('baby_AES.sage')
# sage: baby_aes(u,u)

F = FiniteField(16, "a")

u = ['0001', '0000', '1010', '1011']

# Decimal to binary
def dec_to_bin(dec):
    return '{:04b}'.format(dec)


# Binary to decimal
def bin_to_dec(bin):
    return int(bin, 2)

def str_to_list(message):
    return message.split(' ')

# Binary to Nibble
def bin_to_nibble(bin):
    return F.fetch_int(bin_to_dec(bin))

# Nibble to binary
def nibble_to_bin(nibble):
    a = 2
    return dec_to_bin(eval(str(nibble).replace('^', '**')))

#inverse elements of binary number
def inverseBin(bin):
    if (bin == '0000'):
        return '0000'
    else:
        nibble = bin_to_nibble(bin)
        return nibble_to_bin(nibble**-1)

#inverse elements of nibble
def inverseNibble(nibble):
    if (nibble == 0):
        return 0
    else:
        return nibble**-1

#state with binary numbers to nibbles
def state_bin_to_nibble(nibbles):
    return [bin_to_nibble(nibbles[0]),bin_to_nibble(nibbles[1]),bin_to_nibble(nibbles[2]),bin_to_nibble(nibbles[3])]

#state with nibbles to binary numbers
def state_nibble_to_bin(nibbles):
    return [nibble_to_bin(nibbles[0]),nibble_to_bin(nibbles[1]),nibble_to_bin(nibbles[2]),nibble_to_bin(nibbles[3])]


def sbox(message):
    if message == '0000':
        return '0110'
    elif message == '0001':
        return '1011'
    elif message == '0010':
        return '0101'
    elif message == '0011':
        return '0100'
    elif message == '0100':
        return '0010'
    elif message == '0101':
        return '1110'
    elif message == '0110':
        return '0111'
    elif message == '0111':
        return '1010'
    elif message == '1000':
        return '1001'
    elif message == '1001':
        return '1101'
    elif message == '1010':
        return '1111'
    elif message == '1011':
        return '1100'
    elif message == '1100':
        return '0011'
    elif message == '1101':
        return '0001'
    elif message == '1110':
        return '0000'
    elif message == '1111':
        return '1000'

def inverseSbox(message):
    if message == '0000':
        return '1110'
    elif message == '0001':
        return '1101'
    elif message == '0010':
        return '0100'
    elif message == '0011':
        return '1100'
    elif message == '0100':
        return '0011'
    elif message == '0101':
        return '0010'
    elif message == '0110':
        return '0000'
    elif message == '0111':
        return '0110'
    elif message == '1000':
        return '1111'
    elif message == '1001':
        return '1000'
    elif message == '1010':
        return '0111'
    elif message == '1011':
        return '0001'
    elif message == '1100':
        return '1011'
    elif message == '1101':
        return '1001'
    elif message == '1110':
        return '0101'
    elif message == '1111':
        return '1010'

def subBytes(nibbles):
    subNibbles = []
    for nibble in nibbles:
        subNibbles.append(sbox(nibble))
    return subNibbles


def shiftRows(nibbles):
    return [nibbles[0], nibbles[3], nibbles[2], nibbles[1]]


#C =(t + 1   t  ) ∈ F16 2×2 multiplied with our shiftedRows nibbles
#   (  t   t + 1)
def mixColumns(nibbles):
    t1 = bin_to_nibble('0011') # t+1
    t2 = bin_to_nibble('0010') # t
    x1 = t1*bin_to_nibble(nibbles[0])+t2*bin_to_nibble(nibbles[1])
    x2 = t2*bin_to_nibble(nibbles[0])+t1*bin_to_nibble(nibbles[1])
    x3 = t1*bin_to_nibble(nibbles[2])+t2*bin_to_nibble(nibbles[3])
    x4 = t2*bin_to_nibble(nibbles[2])+t1*bin_to_nibble(nibbles[3])
    return [nibble_to_bin(x1), nibble_to_bin(x2), nibble_to_bin(x3), nibble_to_bin(x4)]


def addRoundKey(nibbles, roundKey):
    x1 = bin_to_nibble(nibbles[0])+bin_to_nibble(roundKey[0])
    x2 = bin_to_nibble(nibbles[1])+bin_to_nibble(roundKey[1])
    x3 = bin_to_nibble(nibbles[2])+bin_to_nibble(roundKey[2])
    x4 = bin_to_nibble(nibbles[3])+bin_to_nibble(roundKey[3])
    return [nibble_to_bin(x1), nibble_to_bin(x2), nibble_to_bin(x3), nibble_to_bin(x4)]


# Only in polynomial form, not Binary!!!!!
# Here I use the key schedule from Small Scale Variants of the AES (SR) Polynomial System Generator:
# https://doc.sagemath.org/html/en/reference/cryptography/sage/crypto/mq/sr.html
# For 3 rounds of Baby-AES -> 4 round keys
def key_schedule(secret_key):
    sr = mq.SR(3, 2, 2, 4, allow_zero_inversions=True)
    E_i = []
    E_i.append(sr.state_array(secret_key))
    j = 0
    for i in range(1, 4):
	    E_i.append(sr.key_schedule(E_i[j],i))
	    j += 1
    return E_i


def baby_aes(message, secret_key):
    roundKeys = key_schedule(state_bin_to_nibble(secret_key))
    rounds = len(roundKeys)
    roundKey0 = [nibble_to_bin(roundKeys[0][0][0]), nibble_to_bin(roundKeys[0][1][0]), nibble_to_bin(roundKeys[0][0][1]), nibble_to_bin(roundKeys[0][1][1])]
    roundKey1 = [nibble_to_bin(roundKeys[1][0][0]), nibble_to_bin(roundKeys[1][1][0]), nibble_to_bin(roundKeys[1][0][1]), nibble_to_bin(roundKeys[1][1][1])]
    roundKey2 = [nibble_to_bin(roundKeys[2][0][0]), nibble_to_bin(roundKeys[2][1][0]), nibble_to_bin(roundKeys[2][0][1]), nibble_to_bin(roundKeys[2][1][1])]
    roundKey3 = [nibble_to_bin(roundKeys[3][0][0]), nibble_to_bin(roundKeys[3][1][0]), nibble_to_bin(roundKeys[3][0][1]), nibble_to_bin(roundKeys[3][1][1])]
    ListRoundKeys = [roundKey0, roundKey1, roundKey2, roundKey3]
    #Rounds R - 1
    u_r = addRoundKey(message, ListRoundKeys[0])
    for i in range(1, rounds - 1):
        u = subBytes(u_r)
        v = shiftRows(u)
        w = mixColumns(v)
        u_r = addRoundKey(w, ListRoundKeys[i])
    #Last Round without mixColumns
    u = subBytes(u_r)
    v = shiftRows(u)
    u_r = addRoundKey(v, ListRoundKeys[3])
    return u_r

def addStates(nibbles, roundKey):
    x1 = bin_to_nibble(nibbles[0])+bin_to_nibble(roundKey[0])
    x2 = bin_to_nibble(nibbles[1])+bin_to_nibble(roundKey[1])
    x3 = bin_to_nibble(nibbles[2])+bin_to_nibble(roundKey[2])
    x4 = bin_to_nibble(nibbles[3])+bin_to_nibble(roundKey[3])
    return [nibble_to_bin(x1), nibble_to_bin(x2), nibble_to_bin(x3), nibble_to_bin(x4)]
    return [nibble_to_bin(x1), nibble_to_bin(x2), nibble_to_bin(x3), nibble_to_bin(x4)]

