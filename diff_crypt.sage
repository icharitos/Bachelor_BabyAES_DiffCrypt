# von Ioannis Charitos
# to launch the attack
#
# sage: load('diff_crypt.sage')
#
#
# to plot the candidate keys in a bar chart
#
# sage: plot_candidate_keys(unfiltered, (1,0,0))
# sage: plot_candidate_keys(new_filtered, (0,1,0))

import time
import matplotlib.pyplot as plt
from sage.plot.bar_chart import BarChart

load('baby_AES.sage')

ListBin = []
for i in range(0, 16):
    ListBin.append(nibble_to_bin(F.fetch_int(i)))

def add_xor(bin1, bin2):
    return nibble_to_bin(bin_to_nibble(bin1)+bin_to_nibble(bin2))


#Step 1: Build the differential distribution table of the S-box

def difference(delta):
    D = []
    for bin1 in ListBin:
        for bin2 in ListBin:
            if (add_xor(bin1, bin2) == delta):
                D.append([bin1, bin2])
    return D

def abs_freq(delta_x):
    Dx = difference(delta_x)
    Dy = []
    frequencies = []
    for x in Dx:
        delta_y = add_xor(sbox(x[0]), sbox(add_xor(x[0],delta_x)))
        Dy.append(delta_y)
    for bin in ListBin:
        frequencies.append([bin, Dy.count(bin)])
    return frequencies


def rel_freq(delta_x):
    freq = []
    abs = abs_freq(delta_x)
    for frequency in abs:
        freq.append([frequency[0], frequency[1]/16])
    return freq


def freq_S(delta_x, delta_y):
    if (delta_y == '0000' or delta_y == '0000'):
        return 1
    freq = []
    abs = abs_freq(delta_x)
    for frequency in abs:
        if(frequency[0] == delta_y):
            return frequency[1]/16


def freq_S_max(delta_x):
    abs = abs_freq(delta_x)
    max = 0
    x = 0
    for freq in abs:
        if(freq[1] > max):
            max = freq[1]
            x = freq[0]
    return x


def freq_S_max_state(delta_x_list):
    max_list = []
    for delta_x in delta_x_list:
        max_list.append(freq_S_max(delta_x))
    return max_list


def diff_distr_table():
    distr_table = []
    for bin in ListBin:
        for freq in abs_freq(bin):
            distr_table.append([bin, freq[0], freq[1]])
    return distr_table



#Step 2: Find a differential trail through R − 1 rounds

def freq_SB(delta_x_list, delta_y_list):
    freq = freq_S(delta_x_list[0], delta_y_list[0]) * freq_S(delta_x_list[1], delta_y_list[1]) * freq_S(delta_x_list[2], delta_y_list[2]) * freq_S(delta_x_list[3], delta_y_list[3])
    return freq


def freq_SR(delta_x_list, delta_y_list):
    if(delta_y_list == shiftRows(delta_x_list)):
        return 1
    else:
        return 0


def freq_MC(delta_x_list, delta_y_list):
    if(delta_y_list == mixColumns(delta_x_list)):
        return 1
    else:
        return 0


def freq_AK(delta_x_list, delta_y_list):
    if(delta_y_list == delta_x_list):
        return 1
    else:
        return 0


def freq_round_r(delta_ur, r):
    if (r == 1 or r == 2):
        max = freq_S_max_state(delta_ur)
        delta_vr = mixColumns(shiftRows(max))
        u_next = freq_S_max_state(delta_vr)
        print(delta_vr, u_next)
        return [delta_vr ,freq_SB(delta_vr, u_next)]
    else:
        max = freq_S_max_state(delta_ur)
        delta_vr = shiftRows(max)
        u_next = freq_S_max_state(delta_vr)
        return [delta_vr ,freq_SB(delta_vr, u_next)]


def freq_rounds(delta_x_list, rounds):
    #Round 0
    delta_u_r = [delta_x_list]
    prop_ratio = 1
    du_r = delta_x_list
    max = freq_S_max_state(du_r)
    prop_ratio = prop_ratio * freq_SB(du_r, max)
    #Round 1 until Rounds-1
    for r in range (1, rounds):
        freq_r = freq_round_r(du_r, r)
        delta_u_r.append(freq_r[0])
        prop_ratio = prop_ratio * freq_r[1]
        du_r = freq_r[0]
    #Round R
    r +=1
    freq_r = freq_round_r(du_r, r)
    return [freq_r[0], prop_ratio]


#Step 3: Nibbles of the last round key

def create_candidate_keys():
    candidates = []
    for nibble in list(F):
        for nibble1 in list(F):
            candidates.append([nibble, nibble1, 0])
    return candidates


def create_all_samples():
    samples = []
    for nibble0 in list(F):
        for nibble1 in list(F):
            for nibble2 in list(F):
                for nibble3 in list(F):
                    samples.append([nibble0, nibble1, nibble2, nibble3])
    return samples


#Creating all 2^16 pairs
def create_samples(secret_key=['0010','0100','0011','1111'], delta_x_list=['0110','0000','0000','1000']):
    S = []
    all_samples = create_all_samples()
    for nibbles in all_samples:
        x1 = state_nibble_to_bin(nibbles)
        y1 = baby_aes(x1, secret_key)
        x2 = addStates(x1, delta_x_list)
        y2 = baby_aes(x2, secret_key)
        S.append([x1,x2,y1,y2])
    return S


def diff_attack(filter, differential=['1010','1100','0000','0000']):
    candidate_keys = create_candidate_keys()
    samples = create_samples()
    start_time = time.time()
    for sample in samples:
        x1 = sample[0]
        x2 = sample[1]
        y1 = sample[2]
        y2 = sample[3]
        if (filter == true and (y1[1] != y2[1] or y1[2] != y2[2])):
            print('discard sample')
        else:
            for i in range(len(candidate_keys)):
                k3_1 = candidate_keys[i][0]
                k3_4 = candidate_keys[i][1]
                w3_1 = add_xor(y1[0], nibble_to_bin(k3_1))
                w3_4 = add_xor(y1[3], nibble_to_bin(k3_4))
                # step 8 undo shiftrows is irrelevant here
                u3_1 = inverseSbox(w3_1)
                u3_2 = inverseSbox(w3_4)
                u1 = [u3_1, u3_2]
                # again for y2
                w3_1 = add_xor(y2[0], nibble_to_bin(k3_1))
                w3_4 = add_xor(y2[3], nibble_to_bin(k3_4))
                # step 8 undo shiftrows is irrelevant here
                u3_1 = inverseSbox(w3_1)
                u3_2 = inverseSbox(w3_4)
                u2 = [u3_1, u3_2]
                delta_u1 = add_xor(u1[0], u2[0])
                delta_u2 = add_xor(u1[1], u2[1])
                if (delta_u1 == differential[0] and delta_u2 == differential[1]):
                    candidate_keys[i][2] += 1
    print("--- %s seconds ---" % (time.time() - start_time))
    return candidate_keys

filtered = diff_attack(true)
unfiltered = diff_attack(false)

def new_filtered(filtered, unfiltered):
    for i in range(0, len(unfiltered)):
        filtered[i][2] = unfiltered[i][2] - filtered[i][2]
    return filtered

def pairs(candidates):
    list = []
    for candidate in candidates:
        list.append([nibble_to_bin(candidate[0]), nibble_to_bin(candidate[1])])
    return list

def counters(candidates):
    list = []
    for candidate in candidates:
        list.append(candidate[2])
    return list

def plot_candidate_keys(candidates, color):
    sorted_candidates = candidates
    sorted_candidates.sort()
    #if list is already sorted, return None, so we check if it's already sorted
    if (sorted_candidates == None):
        sorted_candidates = candidates
    counter = counters(sorted_candidates)
    pair = pairs(sorted_candidates)
    return bar_chart(counter, rgbcolor=color)

new_filtered = new_filtered(filtered, unfiltered)

#plot_candidate_keys(unfiltered, (1,0,0))
#plot_candidate_keys(new_filtered, (0,1,0))
